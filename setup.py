from setuptools import setup
from os.path import join, dirname


setup(
    name='parse_mails',
    version='1.0',
    author='Live free or die',
    author_email='live-freeor-die@yandex.ru',
    license='No',
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    packages=['parse_mails'],
)
