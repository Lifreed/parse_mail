import unittest
from parse_mail import *


found = [r': (?P<id>[\w\d]+):.+from=<(?P<mail>[\w\d@.-]+)>,', r': (?P<id>[\w\d]+):']
found_id = [['id', 'mail'], ['id']]
check = ['status=sent', 'remove']
string = 'Jul 10 10:09:08 srv24-s-st postfix/qmgr[3043]: 25E6CDF04F4: from=<krasteplokomplekt@yandex.ru>, size=617951, nrcpt=1 (queue active)'

class Test(unittest.TestCase):
	def test_found_in_string(self):
		Mail = ParseMails(found, found_id, check)
		mail = Mail.found_in_string(string, found[0], found_id[0])
		self.assertEqual(mail, ['25E6CDF04F4', 'krasteplokomplekt@yandex.ru'])

	def test_new_e_mail(self):
		Mail = ParseMails(found, found_id, check)
		Mail.new_e_mail(string)
		mail = Mail.e_mails
		self.assertEqual(mail, {'krasteplokomplekt@yandex.ru': {'successful': 0, 'failed': 0}})

	def test_new_id_of_mail(self):
		Mail = ParseMails(found, found_id, check)
		Mail.new_id_of_mail(string)
		mail_id = Mail.id_of_mail
		self.assertEqual(mail_id, {'25E6CDF04F4': 'krasteplokomplekt@yandex.ru'})

	def test_remove_in_id_of_mail(self):
		Mail = ParseMails(found, found_id, check)
		Mail.update(string)
		Mail.remove_in_id_of_mail(': 25E6CDF04F4: status=sent')
		self.assertEqual(Mail.id_of_mail, {})
		self.assertEqual(Mail.e_mails, {'krasteplokomplekt@yandex.ru': {'successful': 1, 'failed': 0}})
		Mail.update(string)
		Mail.remove_in_id_of_mail(': 25E6CDF04F4: remove')
		self.assertEqual(Mail.id_of_mail, {})
		self.assertEqual(Mail.e_mails, {'krasteplokomplekt@yandex.ru': {'successful': 1, 'failed': 1}})

	def test_update(self):
		Mail = ParseMails(found, found_id, check)
		Mail.update(string)
		Mail.update(': 1337: from=<live-freeor-die@yandex.ru>,')
		self.assertEqual(Mail.id_of_mail, {'25E6CDF04F4': 'krasteplokomplekt@yandex.ru', '1337': 'live-freeor-die@yandex.ru'})
		self.assertEqual(Mail.e_mails, {'krasteplokomplekt@yandex.ru': {'successful': 0, 'failed': 0}, 'live-freeor-die@yandex.ru': {'successful': 0, 'failed': 0}})
		Mail.update(': 25E6CDF04F4: remove')
		Mail.update(': 1337: status=sent')
		self.assertEqual(Mail.id_of_mail, {})
		self.assertEqual(Mail.e_mails, {'krasteplokomplekt@yandex.ru': {'successful': 0, 'failed': 1}, 'live-freeor-die@yandex.ru': {'successful': 1, 'failed': 0}})

	def test_success_in_id_in_mail(self):
		Mail = ParseMails(found, found_id, check)
		Mail.update(string)
		Mail.success_in_id_in_mail('25E6CDF04F4')
		self.assertEqual(Mail.id_of_mail, {})
		self.assertEqual(Mail.e_mails, {'krasteplokomplekt@yandex.ru': {'successful': 1, 'failed': 0}})

	def test_failed_in_id_in_mail(self):
		Mail = ParseMails(found, found_id, check)
		Mail.update(string)
		Mail.failed_in_id_in_mail('25E6CDF04F4')
		self.assertEqual(Mail.id_of_mail, {})
		self.assertEqual(Mail.e_mails, {'krasteplokomplekt@yandex.ru': {'successful': 0, 'failed': 1}})


if __name__ == '__main__':
    unittest.main()
