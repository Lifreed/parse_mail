import re


class ParseMails:
	def __init__(self, found, found_id, check):
		self.found = found
		self.found_id = found_id
		self.check = check
		self.e_mails = {}
		self.id_of_mail = {}

	def found_in_string(self, string, substring, group):
		match = re.search(substring, string)
		sub = []
		if match:
			for g in group:
				sub.append(match.group(g))
		return sub

	def new_e_mail(self, string):
		mail = self.found_in_string(string, self.found[0], self.found_id[0])
		if self.new_id_of_mail(string) and mail[1] not in self.e_mails:
			self.e_mails[mail[1]] = {'successful': 0, 'failed': 0}

	def new_id_of_mail(self, string):
		mail = self.found_in_string(string, self.found[0], self.found_id[0])
		if mail != []:
			self.id_of_mail[mail[0]] = mail[1]
			return True
		return False

	def success_in_id_in_mail(self, mail_id):
		if mail_id in self.id_of_mail:
			self.e_mails[self.id_of_mail[mail_id]]['successful'] += 1
			self.id_of_mail.pop(mail_id)

	def failed_in_id_in_mail(self, mail_id):
		if mail_id in self.id_of_mail:
			self.e_mails[self.id_of_mail[mail_id]]['failed'] += 1
			self.id_of_mail.pop(mail_id)

	def remove_in_id_of_mail(self, string):
		mail_id = self.found_in_string(string, self.found[1], self.found_id[1])
		check_success = re.search(self.check[0], string)
		check_failed = re.search(self.check[1], string)
		if mail_id != [] and check_success:
			self.success_in_id_in_mail(mail_id[0])
		elif mail_id != [] and check_failed:
			self.failed_in_id_in_mail(mail_id[0])

	def update(self, string):
		self.new_e_mail(string)
		self.remove_in_id_of_mail(string)


if __name__ == '__main__':
	found = [r': (?P<id>[\w\d]+):.+from=<(?P<mail>[\w\d@.-]+)>,', r': (?P<id>[\w\d]+):']
	found_id = [['id', 'mail'], ['id']]
	check = ['status=sent', 'remove']
	Mail = ParseMails(found, found_id, check)
	with open('maillog') as f:
		for line in f.readlines():
			Mail.update(line)
	mails = Mail.e_mails
	for mail, stat in mails.items():
			print('From {0} sent {1} mails and defeat mails {2}'.format(mail, stat['successful'], stat['failed']))
	file = open('output', 'w')
	for mail, stat in mails.items():
		file.writelines('From {0} sent {1} mails and defeat mails {2}\n'.format(mail, stat['successful'], stat['failed']))
